# TEST ONLINE CENDANA2000



## Installation

- Composer install
- Change .env.example to .env (set youre databse name ("cendana2000"), username, and password)
- php -S localhost:8000 -t public
- open in browser localhost:8000

## Database
dumpdatabse : cendana2000.sql
- Create Database Name "cendana2000" in MySql
- php artisan migrate

## Submit Form
![alt text](img/soal1.png)

## API
### Create User Using Sentence
Method : POST
Endpoint : http://localhost:8000/user
parameter : 'kalimat' : "muhammad wijdan 28 depok"

### Show All User
Method : GET
Endpoint : http://localhost:8000/users

### Show Spesific User
Method : GET
Endpoint : http://localhost:8000/user/{id}
![alt text](img/soal1-get.png)

### Update User
Method : PUT
Endpoint : http://localhost:8000/user/{id}
![alt text](img/soal1-update.png)

### Delete User
Method : DELETE
Endpoint : http://localhost:8000/user/{id}
![alt text](img/soal1-delete.png)

### Create & Update Employee
Method : POST
Endpoint : http://localhost:8000/employee
parameter : using JSON

#### Create Employee Screenshoot
![alt text](img/soal2-create.png)

#### Create Employee Screenshoot
![alt text](img/soal2-ifupdate.png)


MUHAMMAD WIJDAN 