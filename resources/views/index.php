<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <title>Muhammad Wijdan - Cendana2000</title>
</head>

<body>
    <div class="container">
        <div class="row justify-content-md-center">
            <div class="col col-lg-6">
                <h3>Test Online Cendana2000</h3>
                <form method="POST" action="#">
                    <div class="mb-3">
                        <label for="exampleInputEmail1" class="form-label">Input Kalimat</label>
                        <input type="text" class="form-control" id="inputKalimat">
                    </div>
                    <button type="button" id="sbmt" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
        <div class="row justify-content-md-center">
            <div class="col col-lg-6" id="result">
                <p id="notifResult">SUKSES! Tersimpan</p>
                <table class="table" >
                    <thead>
                        <th>ID</th>
                        <th>Nama</th>
                        <th>Umur</th>
                        <th>Kota</th>
                    </thead>
                    <tbody id="showResult">

                    </tbody>

                </table>
            </div>
        </div>
    </div>

    <!-- Optional JavaScript; choose one of the two! -->
    <script type="text/javascript">
        $(document).ready(function () {
            $("#result").hide();
            $("#notifResult").hide();
            $.ajax({
                    url: '/users',
                    type: 'GET',
                    success: function (response) {
                        var trHTML = '';
                        $.each(response, function (i, item) {
                            trHTML += '<tr><td>' + item.id + '</td><td>' + item
                                .name + '</td><td>' + item.umur + '</td><td>' + item.kota + '</td></tr>';
                        });
                        $('#showResult').append(trHTML);
                        $("#result").show();
                    }
                });
            $("#sbmt").click(function () {
                $("#result").show();
                var kalimat = $("#inputKalimat").val();
                
                $.ajax({
                    url: '/user',
                    type: 'POST',
                    data: {
                        kalimat: kalimat
                    },
                    success: function (response) {
                        console.log(response)
                        var trHTML = '';
                       
                        trHTML += '<tr><td>' + response.id + '</td><td>' + response
                                .name + '</td><td>' + response.umur + '</td><td>' + response.kota + '</td></tr>';
                        
                        $('#showResult').append(trHTML);
                        $("#notifResult").show();
                    }
                });
            });
        });
    </script>
    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-U1DAWAznBHeqEIlVSCgzq+c9gqGAJn5c/t99JyeKa9xxaYpSvHU5awsuZVVFIhvj" crossorigin="anonymous">
    </script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.3/dist/umd/popper.min.js" integrity="sha384-eMNCOe7tC1doHpGoWe/6oMVemdAVTMs2xqW4mwXrXsW0L84Iytr2wi5v2QjrP/xp" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.min.js" integrity="sha384-cn7l7gDp0eyniUwwAZgrzD06kc/tftFf19TOAs2zVinnD/C7E91j9yyk5//jjpt/" crossorigin="anonymous"></script>
    -->
</body>

</html>