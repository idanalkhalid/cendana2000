<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return view('index', ['name' => 'James']);
});

$router->post('/user','UserController@create');
$router->get('/users','UserController@view');
$router->get('/user/{id}','UserController@show');
$router->put('/user/{id}','UserController@update');
$router->delete('/user/{id}','UserController@destroy');

$router->post('/employee','EmployeeController@create');
$router->get('/employees','EmployeeController@view');
$router->get('/employee/{id}','EmployeeController@show');
$router->put('/employee/{id}','EmployeeController@update');
$router->delete('/employee/{id}','EmployeeController@destroy');

