<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\User;

class UserController extends Controller
{
    public function view(){
        $users= User::get();
        return response()->json($users);
    }
    public function create(Request $request){
        $nama = "";
        $umur = 0;
        $kota = "";

        $kalimat = $request->kalimat;
        $split = explode(" ", $kalimat);

        if(count($split)<=5){
            $nama = $split[0];
        
            $umur = intval($split[1]);
            if($umur==0){
                $nama = $nama." ".$split[1];    
                $umur = intval($split[2]);
                $kota = $split[3];
                if(array_key_exists(4,$split)){
                    $kota = $kota." ".$split[4];
                }              
            }
            $kota = $split[2];
            if(array_key_exists(3,$split)){
                 $kota = $kota." ".$split[3];
            }  
        }
        
        $user = User::create([
            'name'=>strtoupper($nama),
            'umur'=>$umur,
            'kota'=>strtoupper($kota)
        ]);
        return response()->json($user);
    }

    public function show($id){
        $user = User::find($id);
        return response()->json($user);
    }

    public function update(Request $request, $id){
        $user = User::find($id);
        if(!$user){
            return response()->json(['message' => 'User not found'], 404);
        }
        $this->validate($request, [
            "name" => "required",
            "umur" => "required",
            "kota" => "required"
        ]);

        $data = $request->all();
        $user->fill($data);
        $user->save();

        return response()->json($user);
    }

    public function destroy($id){
        $user = User::find($id);
        
        if (!$user) {
            return response()->json(['message' => 'User not found'], 404);
        }

        $user->delete();

        return response()->json(['message' => 'User deleted successfully'], 200);
    }
}
