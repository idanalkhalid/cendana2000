<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Models\Employee;

class EmployeeController extends Controller
{
    public function view(){
        $employees= Employee::get();
        return response()->json($employees);
    }
    public function create(Request $request){
        $data = $request->getContent();
        $data = json_decode($data, true);
        // dd(gettype($data));
        foreach($data as $key => $value){
            $user_exist = Employee::find($value["id"]);

            if($user_exist){
                $dataupdate = array();
                
                if($user_exist->name!=$value["name"]){
                    $data[$key]["name"]= $value['name']." (Data telah diubah)";
                    $dataupdate["name"] = $value["name"];
                }

                if($user_exist->username!=$value["username"]){
                    $data[$key]["username"]= $value['username']." (Data telah diubah)";
                    $dataupdate["username"] = $value["username"];
                }

                if($user_exist->email!=$value["email"]){
                    $data[$key]["email"]= $value['email']." (Data telah diubah)";
                    $dataupdate["email"] = $value["email"];
                }

                if($user_exist->phone!=$value["phone"]){
                    $data[$key]["phone"]= $value['phone']." (Data telah diubah)";
                    $dataupdate["phone"] = $value["phone"];
                }

                if($user_exist->phone!=$value["phone"]){
                    $data[$key]["phone"]= $value['phone']." (Data telah diubah)";
                    $dataupdate["phone"] = $value["phone"];
                }
                // $address_temp = json_decode($user_exist->address);
                
                // if($address_temp->street!=$value['address']['street']){
                //     $data[$key]["address"]['street'] = $value['address']['street']." (Data telah diubah)";
                //     $dataupdate["address"]['street'] = $value["address"]['street'];
                // }

                // if($address_temp->suite!=$value['address']['suite']){
                //     $data[$key]["address"]['suite'] = $value['address']['suite']." (Data telah diubah)";
                //     $dataupdate["address"]['suite'] = $value["address"]['suite'];
                // }

                // if($address_temp->suite!=$value['address']['city']){
                //     $data[$key]["address"]['city'] = $value['address']['city']." (Data telah diubah)";
                //     $dataupdate["address"]['city'] = $value["address"]['city'];
                // }

                // if($address_temp->suite!=$value['address']['zipcode']){
                //     $data[$key]["address"]['zipcode'] = $value['address']['zipcode']." (Data telah diubah)";
                //     $dataupdate["address"]['zipcode'] = $value["address"]['zipcode'];
                // }
                
                // $geo_temp = $address_temp->geo;

                // if($geo_temp->lat!=$value['address']['geo']['lat']){
                //     $data[$key]["address"]['geo']['lat'] = $value['address']['geo']['lat']." (Data telah diubah)";
                //     $dataupdate["address"]['geo']['lat'] = $value["address"]['geo']['lat'];
                // }
                

                // if($user_exist->address!=json_encode($value["address"],)){
                //     $data[$key]["address"]= $value['address']." (Data telah diubah)";
                //     $dataupdate["address"] = $value["address"];
                // }

                // if($user_exist->company!=$value["company"]){
                //     $data[$key]["company"]= $value['company']." (Data telah diubah)";
                //     $dataupdate["company"] = $value["company"];
                // }

                $user_exist->fill($dataupdate);
                $user_exist->save();

            }else{
                $data[$key] = Employee::create([
                    'name' => $value['name'], 
                    'username' => $value['username'], 
                    'email' => $value['email'], 
                    'address' => json_encode($value['address'],true), 
                    'phone' => $value['phone'], 
                    'website' => $value['website'], 
                    'company' => json_encode($value['company'],true)
                ]);
            
            }
            
        }
        // Employee::insert($data);
        return response()->json($data);
    }

    public function show($id){
        $employee = Employee::find($id);
        return response()->json($employee);
    }

    public function update(Request $request, $id){
        $employee = Employee::find($id);
        if(!$employee){
            return response()->json(['message' => 'Employee not found'], 404);
        }
        $this->validate($request, [
            "name" => "required",
            "umur" => "required",
            "kota" => "required"
        ]);

        $data = $request->all();
        $employee->fill($data);
        $employee->save();

        return response()->json($employee);
    }

    public function destroy($id){
        $employee = Employee::find($id);
        
        if (!$employee) {
            return response()->json(['message' => 'Employee not found'], 404);
        }

        $employee->delete();

        return response()->json(['message' => 'Employee deleted successfully'], 200);
    }
}
